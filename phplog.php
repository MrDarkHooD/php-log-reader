<?php
/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details. */

/* Creator: https://gitlab.com/MrDarkHooD */

$types = ["warn", "error", "notice", "crit"];
if(isset($_GET["submit"])) {
	$types = [];
	if(isset($_GET["warn"])) $types[] = "warn";
	if(isset($_GET["error"])) $types[] = "error";
	if(isset($_GET["notice"])) $types[] = "notice";
	if(isset($_GET["crit"])) $types[] = "crit";
}

$log = file_get_contents("/var/log/nginx/error.log");
$log = explode("\n", $log);
$log = array_reverse($log);

$limit = 255;
if(isset($_GET["limit"]))
	if(is_numeric($_GET["limit"]))
		$limit = round($_GET["limit"]);
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>PHP error log</title>
    	<link rel="stylesheet" type="text/css" href="theme.css" /> 
	</head>
	<body>
		<div class="search">
			<form method="get">
				warn: <input type="checkbox" name="warn" value="1"<?php if(in_array("warn", $types)) echo " checked"; ?>><br>
				error: <input type="checkbox" name="error" value="1"<?php if(in_array("error", $types)) echo " checked"; ?>><br>
				notice: <input type="checkbox" name="notice" value="1"<?php if(in_array("notice", $types)) echo " checked"; ?>><br>
				crit: <input type="checkbox" name="crit" value="1"<?php if(in_array("crit", $types)) echo " checked"; ?>><br>
				limit: <input type="number" name="limit" min="0" max="255" value="<?php echo $limit; ?>"><br>
				<input type="submit" value="Search" name="submit">
			</form>
		</div>
		<div class="info">
			PHP version: <?php echo explode("-", phpversion())[0]; ?><br>
			<?php echo shell_exec('nginx -v 2>&1'); ?>
		</div>
<?php	
$i = 0;
foreach($log as $line) {
	if(!$line) continue;
	preg_match('/^\d+\/\d+\/\d+\s\d+:\d+:\d+\s\[(.*)\]/', $line, $type);
	$line = htmlspecialchars($line);
	$line = preg_replace("/\s\[(.*)\]\s(\d{1,6}#\d{1,6}):/", " [$1] $2<br>\n", $line);
	$line = preg_replace("/([0-9])PHP/", "$1<br>\nPHP", $line);
	$line = preg_replace("/PHP\smessage:\s(.*):\s(.*)\sin\s\/(.*)\son\sline\s(\d{1,10})/m", "PHP message: $1:<br>\n <b>$2</b> in <u><b>/$3</u> on <u>line $4</b></u>", $line);
		
	if(isset($type[1])) {
		if(in_array($type[1], $types)) {
			echo "\t\t<div class='".$type[1]." log'>\n";
			echo $line."\n";
			echo "\t\t</div>\n";
			$i++;
		}
	}elseif(count($types) == 4) {
		echo "<div>".$line."</div>";
		$i++;
	}
	if($i >= $limit) break;
}
?>
	</body>
</html>